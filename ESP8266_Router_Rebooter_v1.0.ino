#include <ESP8266Ping.h>


#include <ESP8266WiFi.h>          //https://github.com/esp8266/Arduino

//needed for library
#include <DNSServer.h>
#include <ESP8266WebServer.h>
#include <WiFiManager.h>          //https://github.com/tzapu/WiFiManager


#define RELAY_PIN D3


const char * remote_host = "www.google.com";
const int numberOfPings = 10;


//Some timing constants
const int minSecondsFromResetToStartup = 20;
const int minSecondsFromResetToNormal = minSecondsFromResetToStartup + 70;
//Some timing FSM variables
volatile int secondsSinceReset = 0;

//FSM state enum
enum machine_state {
  powerCycle, startupMode, normalMode, normalToPowerCycle, powerCycleToStartup, startupToNormal
  
  };

machine_state boardState = startupMode;

//for LED status
#include <Ticker.h>
Ticker ticker;

Ticker secondsTicker;

void secondsTick(){
  secondsSinceReset++;
  
  }

void tick()
{
  //toggle state
  int state = digitalRead(BUILTIN_LED);  // get the current state of GPIO1 pin
  digitalWrite(BUILTIN_LED, !state);     // set pin to the opposite state
}

//gets called when WiFiManager enters configuration mode
void configModeCallback (WiFiManager *myWiFiManager) {
  Serial.println("Entered config mode");
  Serial.println(WiFi.softAPIP());
  //if you used auto generated SSID, print it
  Serial.println(myWiFiManager->getConfigPortalSSID());
  //entered config mode, make led toggle faster
  ticker.attach(0.2, tick);
}

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  
  //set led pin as output
  pinMode(BUILTIN_LED, OUTPUT);
  // start ticker with 0.5 because we start in AP mode and try to connect
  ticker.attach(0.6, tick);

  pinMode(RELAY_PIN, OUTPUT);
  
  //WiFiManager
  //Local intialization. Once its business is done, there is no need to keep it around
  WiFiManager wifiManager;
  //reset settings - for testing
  //wifiManager.resetSettings();

  //set callback that gets called when connecting to previous WiFi fails, and enters Access Point mode
  wifiManager.setAPCallback(configModeCallback);

  //fetches ssid and pass and tries to connect
  //if it does not connect it starts an access point with the specified name
  //here  "AutoConnectAP"
  //and goes into a blocking loop awaiting configuration
  if (!wifiManager.autoConnect()) {
    Serial.println("failed to connect and hit timeout");
    //reset and try again, or maybe put it to deep sleep
    ESP.reset();
    delay(1000);
  }

  //if you get here you have connected to the WiFi
  Serial.println("connected...yeey :)");
  ticker.detach();
  //keep LED on
  digitalWrite(BUILTIN_LED, LOW);



  secondsTicker.attach(1, secondsTick);
}

void loop() {
  bool rebootNow = false;
  Serial.print("Seconds since last reset: ");
  Serial.println(secondsSinceReset);
  char debugChar = 'x';
  if (Serial.available()) debugChar = Serial.read();
  switch (debugChar){
    case '1':
    boardState = normalMode;
    break;
    case '0':
    secondsSinceReset = 0;
    boardState = powerCycle;
    break;
    default:
    break;
    }

  switch (boardState){
    case startupMode:
      //Drive relay pin LOW, so that circuit is closed
      digitalWrite(RELAY_PIN, LOW);

      if (secondsSinceReset >= minSecondsFromResetToNormal){
        boardState = normalMode;
        ticker.detach();
        Serial.println("Beginning polling/normal mode");
        }
    break;
    case normalMode:
      //Normal mode, no light      
      digitalWrite(BUILTIN_LED, HIGH);
      //Normal mode, relay pin LOW for closed circuit
      digitalWrite(RELAY_PIN, LOW);

      //Now try to ping google:
      if (!WiFi.isConnected()){
        Serial.println("Not connected!");
        rebootNow = true;        
      } else {
        Serial.print("Connected! ");
  
        if (Ping.ping(remote_host, numberOfPings)){
          int avTimeMs = Ping.averageTime();
          Serial.print("Pinged ");
          Serial.print(remote_host);
          Serial.print(" successfully! Average of response time of ");
          Serial.print(numberOfPings);
          Serial.print(": ");
          Serial.print(avTimeMs);
          Serial.print(" milliseconds!");
          } else {
            Serial.print("Couldn't ping ");
            Serial.print(remote_host);
            rebootNow = true;
            }
          Serial.println();
      }
      if (rebootNow){
        secondsSinceReset = 0;
        boardState = powerCycle;
        Serial.println("Rebooting router...");
        }
    break;
    case powerCycle:
      //Probably do solid light
      digitalWrite(BUILTIN_LED, LOW);
      //Drive relay pin HIGH to open circuit
      digitalWrite(RELAY_PIN, HIGH);

      //If seconds since last reset exceed the chosen number, change to startup mode
      if (secondsSinceReset >= minSecondsFromResetToStartup){
        Serial.println("Beginning Startup Mode...");
        boardState = startupMode;
        ticker.attach(0.5, tick);
        }
    break;
    default:
      boardState = startupMode;
    break;
    
    }
  
  delay(200);
}
